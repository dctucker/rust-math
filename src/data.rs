#![allow(dead_code)]
extern crate rand;

use data::rand::prelude::*;
use data::rand::distributions::StandardNormal;

pub fn generate_test_data(_beta: i32, n: i32, _std_dev: i32) -> (Vec<i32>, Vec<i32>) {
    let mut rng = thread_rng();
    let mut _x: Vec<i32> = Vec::with_capacity(n as usize);

    for _i in 0..n {
        _x.push(rng.gen_range(0, 100));
    }

    let mut _y: Vec<i32> = Vec::with_capacity(n as usize);
    let mut _normal: f64;
    for _i in 0..n {
        _normal = SmallRng::from_entropy().sample(StandardNormal);
        _y.push( ((_x[_i as usize] * _beta) as f64 + _normal * _std_dev as f64) as i32);
    }

    (_x, _y)

}