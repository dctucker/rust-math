mod math;
mod data;


fn main() {
    /*
    let mut vec1 = vec![1, 5, 10, 2, 15, 17, 3];
    let mut vec2 = vec![1, 5, 10, 2, 15, 17];

    let mut _a = math::avg(&mut vec1);
    println!("average odd: {}", _a);

    _a = math::avg(&mut vec2);
    println!("average even: {}", _a);

    _a = math::median(&mut vec1);
    println!("median odd: {}", _a);

    _a = math::median(&mut vec2);
    println!("median even: {}", _a);

    _a = math::stdev_p(&mut vec1);
    println!("stdev_s odd: {}", _a);

    _a = math::stdev_s(&mut vec2);
    println!("stdev_p even: {}", _a);

    _a = math::variance_p(&mut vec1);
    println!("variance_p odd: {}", _a);

    _a = math::variance_s(&mut vec2);
    println!("variance_s even: {}", _a);
    */
    let beta = 100;
    let n = 10000000;
    let std_dev = 1000;

    let (mut x, mut y) = data::generate_test_data(beta, n, std_dev);

    /*
    println!(" x\t y");
    for _i in 0..n {
        println!("{}\t{}", x[_i as usize], y[_i as usize]);

    }
    */

    let (a, b, r2) = math::ols(&mut x, &mut y);

    println!("\n");
    println!("FIT [a : {}, b: {}, r2: {}]", a, b, r2);
    println!("\n");/*
    println!(" x\t y");
    let m = x.iter().max().unwrap().clone() as i64;
    let scale = m / 10;
    for _i in 1..=10 {
        println!("{}\t{}", (_i * scale), b * (_i * scale) as f64 + a);

    }
    */

}