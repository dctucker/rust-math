#![allow(dead_code)]

pub fn avg(v: &[i32]) -> f64 {
    let mut i: i64 = 0;

    for num in v.iter() {
        i += num.clone() as i64;
    }

    i as f64 / v.len() as f64

}


/// calculate the median of a vector
/// if the vector is odd in length the return the middle o
pub fn median(v: &mut [i32]) -> f64 {
    let _vec = v.sort();
    let length = v.len();
    if length % 2 == 1 {
        v[length / 2] as f64
    } else {
        let i1 = v[length / 2];
        let i2 = v[(length / 2) + 1];
        (i1 + i2) as f64 / 2.0

    }

}


/// create the standard deviation of the sample
pub fn stdev_s(v: &mut [i32]) -> f64 {
    variance_s(v).sqrt()

}


/// create the standard deviation of the populaton
pub fn stdev_p(v: &mut [i32]) -> f64 {
    variance_p(v).sqrt()

}


/// create the variance of the sample
pub fn variance_s(v: &mut [i32]) -> f64 {
    sum_of_squares(v) / df(v, 1)

}


/// create the variance of the population
pub fn variance_p(v: &mut [i32]) -> f64 {
    sum_of_squares(v) / df(v, 0)

}


/// create sums of squares
pub fn sum_of_squares(v: &mut [i32]) -> f64 {
    let average = avg(v);
    let mut i = 0.0;
    for num in v.iter() {
        i += (*num as f64 - average).powi(2);
    }
    i

}


/// calculate the degrees of freedom
fn df(v: &mut [i32], deg: i32) -> f64 {
    (v.len() as i32 - deg) as f64

}

fn sum_product(x: &mut [i32], y: &mut [i32]) -> f64 {
    let mut _sp: f64 = 0.0;
    for _i in 0..x.len() {
        _sp += x[_i] as f64 * y[_i] as f64
    }
    _sp as f64
}

pub fn ols(x: &mut Vec<i32>, y: &mut Vec<i32>) -> (f64, f64, f64) {
    let _sum_of_squares_xx = sum_of_squares(x);
    let _sum_of_squares_yy = sum_of_squares(y);
    let _avg_x = avg(x);
    let _avg_y = avg(y);
    let _sum_of_squares_xy = sum_product(x, y) - (x.len() as f64 * _avg_x * _avg_y);
    let _b = _sum_of_squares_xy / _sum_of_squares_xx;
    let _a = _avg_y - _b * _avg_x;
    let _r2 = _sum_of_squares_xy.powi(2)/(_sum_of_squares_xx * _sum_of_squares_yy);

    (_a, _b, _r2)
}